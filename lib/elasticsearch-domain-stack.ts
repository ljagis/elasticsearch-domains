import * as cdk from "@aws-cdk/core";
import * as ec2 from "@aws-cdk/aws-ec2";
import * as es from "@aws-cdk/aws-elasticsearch";

import { ElasticsearchAdminGroup } from "./elasticsearch-admin-group";
import { ElasticsearchAlarmsConstruct } from "./elasticsearch-alarms-construct";

interface ElasticsearchDomainProps extends cdk.StackProps {
  prefix: string;
  stage: "dev" | "prod";
}

export class ElasticsearchDomainStack extends cdk.Stack {
  public domain: es.Domain;
  public prefix: string;
  public stage: "dev" | "prod";

  constructor(
    scope: cdk.Construct,
    id: string,
    props: ElasticsearchDomainProps
  ) {
    super(scope, id, props);

    this.prefix = props.prefix;
    this.stage = props.stage;

    if (props.stage === "dev") {
      this.domain = new es.Domain(this, "DevElasticsearchDomain", {
        domainName: `${props.prefix}-${props.stage}`,
        version: es.ElasticsearchVersion.V7_10,
        enableVersionUpgrade: true,
        capacity: {
          masterNodes: 0,
          dataNodes: 1,
          dataNodeInstanceType: "t3.small.elasticsearch",
        },
        ebs: {
          enabled: true,
          iops: 0,
          volumeType: ec2.EbsDeviceVolumeType.GP2,
          volumeSize: 10,
        },
      });
    }

    if (props.stage === "prod") {
      this.domain = new es.Domain(this, "ProdElasticsearchDomain", {
        domainName: `${props.prefix}-${props.stage}`,
        version: es.ElasticsearchVersion.V7_10,
        enableVersionUpgrade: true,
        capacity: {
          masterNodes: 3,
          masterNodeInstanceType: "c5.large.elasticsearch",
          dataNodes: 2,
          dataNodeInstanceType: "m5.large.elasticsearch",
        },
        ebs: {
          enabled: true,
          iops: 0,
          volumeType: ec2.EbsDeviceVolumeType.GP2,
          volumeSize: 10,
        },
        zoneAwareness: {
          availabilityZoneCount: 2,
        },
      });

      new ElasticsearchAlarmsConstruct(this, "ElasticsearchAlarms");
    }

    new ElasticsearchAdminGroup(this, "ElasticsearchAdminAccessGroup", {
      groupName: `${props.prefix}-${props.stage}-AdminAccessGroup`,
    });
  }
}
