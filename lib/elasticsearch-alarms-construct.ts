import * as cdk from "@aws-cdk/core";
import * as cloudwatch from "@aws-cdk/aws-cloudwatch";
import * as cw_actions from "@aws-cdk/aws-cloudwatch-actions";
import * as sns from "@aws-cdk/aws-sns";

import { ElasticsearchDomainStack } from "./elasticsearch-domain-stack";

const ES_ALARMS_TOPIC_ARN = "arn:aws:sns:us-east-1:125305802877:NotifySlack";

export class ElasticsearchAlarmsConstruct extends cdk.Construct {
  constructor(scope: ElasticsearchDomainStack, id: string) {
    super(scope, id);

    const dimensions: cloudwatch.DimensionHash = {
      DomainName: scope.domain.domainName,
      ClientId: scope.account,
    };

    const alarms = [
      /**
       * ClusterStatus.red maximum is >= 1 for
       * 1 minute, 1 consecutive time
       *
       * At least one primary shard and its replicas are not allocated to a node.
       * See https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/aes-handling-errors.html#aes-handling-errors-red-cluster-status
       */
      new cloudwatch.Alarm(scope, "ElasticsearchRedStatusAlarm", {
        alarmName: `${scope.prefix}-${scope.stage}-ESRedStatusAlarm`,
        alarmDescription:
          "At least one primary shard and its replicas are not allocated to a node.",
        metric: scope.domain.metricClusterStatusRed({
          period: cdk.Duration.minutes(1),
          statistic: "max",
          dimensions,
        }),
        threshold: 1,
        evaluationPeriods: 1,
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }),

      /**
       * ClusterStatus.yellow maximum is >= 1
       * for 1 minute, 1 consecutive time
       *
       * At least one replica shard is not allocated to a node.
       * See https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/aes-handling-errors.html#aes-handling-errors-yellow-cluster-status
       */
      new cloudwatch.Alarm(scope, "ElasticsearchYellowStatusAlarm", {
        alarmName: `${scope.prefix}-${scope.stage}-ESYellowStatusAlarm`,
        alarmDescription:
          "At least one replica shard is not allocated to a node.",
        metric: scope.domain.metricClusterStatusYellow({
          period: cdk.Duration.minutes(1),
          statistic: "max",
          dimensions,
        }),
        threshold: 1,
        evaluationPeriods: 1,
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }),

      /**
       * FreeStorageSpace minimum is <= 25% of the storage space for each node
       * for 1 minute, 1 consecutive time
       *
       * A node in your cluster is down to 2 GiB of free storage space.
       * See https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/aes-handling-errors.html#aes-handling-errors-watermark
       */
      new cloudwatch.Alarm(scope, "ElasticsearchLowStorageSpaceAlarm", {
        alarmName: `${scope.prefix}-${scope.stage}-ESLowStorageSpaceAlarm`,
        alarmDescription:
          "A node in your cluster is down to 2 GiB of free space.",
        metric: scope.domain.metricFreeStorageSpace({
          period: cdk.Duration.minutes(1),
          statistic: "min",
          dimensions,
        }),
        threshold: 2048,
        evaluationPeriods: 1,
        comparisonOperator:
          cloudwatch.ComparisonOperator.LESS_THAN_OR_EQUAL_TO_THRESHOLD,
      }),

      /**
       * ClusterIndexWritesBlocked is >= 1 for
       * 5 minutes, 1 consecutive time
       *
       * Your cluster is blocking write requests.
       * See https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/aes-handling-errors.html#troubleshooting-cluster-block
       */
      new cloudwatch.Alarm(scope, "ElasticsearchIndexWritesBlockedAlarm", {
        alarmName: `${scope.prefix}-${scope.stage}-ESIndexWritesBlockedAlarm`,
        alarmDescription: "Your cluster is blocking write requests.",
        metric: scope.domain.metricClusterIndexWritesBlocked({
          period: cdk.Duration.minutes(5),
          statistic: "max",
          dimensions,
        }),
        threshold: 1,
        evaluationPeriods: 1,
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }),

      /**
       * Nodes minimum is < the number of nodes in your cluster
       * for 1 day, 1 consecutive time
       *
       * At least one node in your cluster has been unreachable for one day.
       * See https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/aes-handling-errors.html#aes-handling-errors-failed-cluster-nodes
       */
      new cloudwatch.Alarm(scope, "ElasticsearchNodeFailedAlarm", {
        alarmName: `${scope.prefix}-${scope.stage}-ESNodeFailedAlarm`,
        alarmDescription:
          "At least one node in your cluster has been unreachable for one day.",
        metric: scope.domain.metricNodes({
          period: cdk.Duration.days(1),
          statistic: "min",
          dimensions,
        }),
        threshold: 5,
        evaluationPeriods: 1,
        comparisonOperator: cloudwatch.ComparisonOperator.LESS_THAN_THRESHOLD,
      }),

      /**
       * AutomatedSnapshotFailure maximum is
       * >= 1 for 1 minute, 1 consecutive time
       *
       * An automated snapshot failed.
       * See https://docs.aws.amazon.com/elasticsearch-service/latest/developerguide/aes-handling-errors.html#aes-handling-errors-red-cluster-status
       */
      new cloudwatch.Alarm(scope, "ElasticsearchSnapshotFailedAlarm", {
        alarmName: `${scope.prefix}-${scope.stage}-ESSnapshotFailedAlarm`,
        alarmDescription: "An automated snapshot failed.",
        metric: scope.domain.metricNodes({
          period: cdk.Duration.minutes(1),
          statistic: "max",
          dimensions,
        }),
        threshold: 1,
        evaluationPeriods: 1,
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }),

      /**
       * CPUUtilization maximum is >=
       * 80% for 15 minutes, 3 consecutive times
       *
       * Your cluster is experiencing sustained high data node CPU usage.
       */
      new cloudwatch.Alarm(scope, "ElasticsearchHighDataCPUAlarm", {
        alarmName: `${scope.prefix}-${scope.stage}-ESHighDataCPUAlarm`,
        alarmDescription:
          "Your cluster is experiencing sustained high data node CPU usage.",
        metric: scope.domain.metricCPUUtilization({
          period: cdk.Duration.minutes(15),
          statistic: "max",
          dimensions,
        }),
        threshold: 80,
        evaluationPeriods: 3,
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }),

      /**
       * JVMMemoryPressure maximum is >=
       * 80% for 15 minutes, 3 consecutive times
       *
       * Your cluster is experiencing sustained high data node JVM memory usage.
       */
      new cloudwatch.Alarm(scope, "ElasticsearchHighDataJVMAlarm", {
        alarmName: `${scope.prefix}-${scope.stage}-ESHighDataJVMAlarm`,
        alarmDescription:
          "Your cluster is experiencing sustained high data node JVM memory usage.",
        metric: scope.domain.metricJVMMemoryPressure({
          period: cdk.Duration.minutes(15),
          statistic: "max",
          dimensions,
        }),
        threshold: 80,
        evaluationPeriods: 3,
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }),

      /**
       * MasterCPUUtilization maximum is >=
       * 50% for 15 minutes, 3 consecutive times
       *
       * Your cluster is experiencing sustained high master node CPU usage.
       */
      new cloudwatch.Alarm(scope, "ElasticsearchHighMasterCPUAlarm", {
        alarmName: `${scope.prefix}-${scope.stage}-ESHighMasterCPUAlarm`,
        alarmDescription:
          "Your cluster is experiencing sustained high master node CPU usage.",
        metric: scope.domain.metricMasterCPUUtilization({
          period: cdk.Duration.minutes(15),
          statistic: "max",
          dimensions,
        }),
        threshold: 50,
        evaluationPeriods: 3,
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }),

      /**
       * MasterJVMMemoryPressure maximum is
       * >= 80% for 15 minutes, 1 consecutive time
       *
       * Your cluster is experiencing sustained high master node JVM memory usage.
       */
      new cloudwatch.Alarm(scope, "ElasticsearchHighMasterJVMAlarm", {
        alarmName: `${scope.prefix}-${scope.stage}-ESHighMasterJVMAlarm`,
        alarmDescription:
          "Your cluster is experiencing sustained high master node JVM memory usage.",
        metric: scope.domain.metricMasterJVMMemoryPressure({
          period: cdk.Duration.minutes(15),
          statistic: "max",
          dimensions,
        }),
        threshold: 80,
        evaluationPeriods: 1,
        comparisonOperator:
          cloudwatch.ComparisonOperator.GREATER_THAN_OR_EQUAL_TO_THRESHOLD,
      }),
    ];

    const action = new cw_actions.SnsAction(
      sns.Topic.fromTopicArn(
        scope,
        "ElasticsearchAlarmsTopic",
        ES_ALARMS_TOPIC_ARN
      )
    );

    alarms.forEach((alarm) => alarm.addAlarmAction(action));
  }
}
