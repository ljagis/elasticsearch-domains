import * as iam from "@aws-cdk/aws-iam";

import { ElasticsearchDomainStack } from "./elasticsearch-domain-stack";

export class ElasticsearchAdminGroup extends iam.Group {
  constructor(
    scope: ElasticsearchDomainStack,
    id: string,
    props?: iam.GroupProps
  ) {
    super(scope, id, props);

    const policy = new iam.Policy(scope, "ElasticsearchAdminAccessPolicy", {
      policyName: `${scope.prefix}-${scope.stage}-AdminAccessPolicy`,
      statements: [
        new iam.PolicyStatement({
          effect: iam.Effect.ALLOW,
          actions: ["es:ESHttp*"],
          resources: [`${scope.domain.domainArn}/*`],
        }),
      ],
    });

    policy.attachToGroup(this);
  }
}
