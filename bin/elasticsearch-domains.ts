#!/usr/bin/env node

import "source-map-support/register";

import * as cdk from "@aws-cdk/core";

import { ElasticsearchDomainStack } from "../lib/elasticsearch-domain-stack";

const app = new cdk.App();

new ElasticsearchDomainStack(app, "DevElasticsearchDomainStack", {
  prefix: "shared-es-domain",
  stage: "dev",
});

new ElasticsearchDomainStack(app, "ProdElasticsearchDomainStack", {
  prefix: "shared-es-domain",
  stage: "prod",
});
